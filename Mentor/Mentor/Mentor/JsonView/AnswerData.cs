﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mentor.JsonView
{
    public class AnswerData
    {
        public int StudentId { get; set; }
        public string AccessToken { get; set; }
        public int TopicId   { get; set; }
        public string StartTestDT { get; set; }
        public string FinishTestDT { get; set; }
        public List<int> TestIds { get; set; } = new List<int>();
        public List<int> Answers   { get; set; } = new List<int>();
        public int Rating    { get; set; }
        public int ErrorCount { get; set; }
        public int Year { get; set; }
        public int Sem { get; set; }
    }
}
