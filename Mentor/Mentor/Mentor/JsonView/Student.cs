﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mentor.JsonView
{
    public class Student : Response
    {
        public string Name { get; set; }
        public string SharedSec { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Email { get; set; }
        public string AccessToken { get; set; }
        public string Version { get; set; }

        public void GenerateTestData()
        {
            this.Id = 60;
            this.Name = "Гаврютин Никита Николаевич";
        }

        public void Set(Student student)
        {
            this.Id         = student.Id;
            this.Name       = student.Name;
            this.Login      = student.Login;
            this.Pass       = student.Pass;
            this.Email      = student.Email;
            this.SharedSec  = student.SharedSec;
            this.AccessToken = student.AccessToken;
            this.Version = student.Version;
        }
    }
}
