﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Mentor.JsonView
{
    public class DiaryRecord
    {
        public string Name  { get; set; }
        public int Value { get; set; }
        public string Date  { get; set; }
        public int Visited { get; set; }
        public int TryCount { get; set; }
        public string TryCountStr
        {
            get
            {
                if (Value == 1)
                    return TryCount.ToString();
                else
                {
                    if (TryCount > 0)
                        return TryCount.ToString();
                    else
                        return String.Empty;
                }
            }
        }
        public Color Result
        {
            get
            {
                if(Value == 1)
                    return Color.Green;
                else
                {
                    if(TryCount > 0)
                        return Color.Yellow;
                    else
                        return Color.Red;
                }
            }
        }
    }

    public class TestResultResponce : Response
    {
        public int Visited { get; set; }
        public int Value { get; set; }
        public int BARS { get; set; }
        public List<DiaryRecord> DiaryRecords { get; set; }
    }

    public class ResultRequest
    {
        public int DisciplineId { get; set; }
        public int StudentId { get; set; }
        public string AccessToken { get; set; }
        public int Year { get; set; }
        public int Sem { get; set; }
    }
}
