﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.JsonView;
using Mentor.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mentor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        private Student     _student;

        public ProfilePage()
        {
            InitializeComponent();

            this.Title = "Профиль";
        }

        private bool _loaded = false;
        public ProfilePage(Student student)
        {
            InitializeComponent();

            this.Appearing += ProfilePage_Appearing;

            _student = student;
        }

        private void ProfilePage_Appearing(object sender, EventArgs e)
        {
            entryCellFio.Text = _student.Name;
            entryCellLogin.Text = _student.Login;
            entryCellEmail.Text = _student.Email;
            entryCellOldPass.Text = "Введите старый пароль";
            entryCellNewPass.Text = "Введите новый пароль";
            entryCellNewPassAgain.Text = "Повторите новый пароль";

            this.Title = "Профиль";
        }

        private void SwitchCell_OnChanged(object sender, ToggledEventArgs e)
        {
            if(switchCell.On == true)
            {
                entryCellOldPass.Text = "";
                entryCellNewPass.Text = "";
                entryCellNewPassAgain.Text = "";
                SetIsEnableControls(true);
            }
            else
            {
                entryCellLogin.Text = _student.Login;
                entryCellEmail.Text = _student.Email;
                entryCellOldPass.Text = "Введите старый пароль";
                entryCellNewPass.Text = "Введите новый пароль";
                entryCellNewPassAgain.Text = "Повторите новый пароль";

                SetIsEnableControls(false);
            }
        }

        private void SetIsEnableControls(bool b)
        {
            entryCellLogin.IsEnabled = b;
            entryCellEmail.IsEnabled = b;
            entryCellNewPass.IsEnabled = b;
            entryCellOldPass.IsEnabled = b;
            entryCellNewPassAgain.IsEnabled = b;
            ok.IsEnabled = b;
            cancel.IsEnabled = b;
        }

        private void OnCancelProfile(object sender, EventArgs e)
        {
            switchCell.On = false;
        }

        private async void OnSaveProfile(object sender, EventArgs e)
        {
            try
            {
                bool canWritePass  = CanWritePass();
                bool canWriteLogin = CanWriteLogin();
                bool canWriteEmail = CanWriteEmail();

                Student student = new Student();
                student.Set(_student);

                if (canWriteLogin == true)
                {
                    student.Login = entryCellLogin.Text;
                }
                if (canWriteEmail == true)
                {
                    student.Email = entryCellEmail.Text;
                }
                if (canWritePass == true)
                {
                    student.Pass = entryCellNewPass.Text;
                }

                if(student.Login != _student.Login ||
                   student.Email != _student.Email ||
                   student.Pass  != _student.Pass    )
                {
                    activityIndicatorGrid.IsVisible = true;
                    Response response = await RestClient.SetNewProfile(student);
                    if (response.Id > -1)
                    {
                        await DisplayAlert("Профиль", "Изменения приняты!", "OK");

                        if (student.Login != _student.Login ||
                             student.Pass != _student.Pass)
                        {
                            bool result = await this.DisplayAlert("Автоподстановка", "Сохранить новые данные авторизации для автоподстановки?", "Да", "Нет");
                            if (result)
                            {
                                AuthorizationRequest request = new AuthorizationRequest();
                                request.Login = student.Login;
                                request.Pass = student.Pass;
                                AutorizationSubstitute.WriteAutorizationData(request);
                            }
                        }

                        _student.Set(student);
                        switchCell.On = false;
                    }
                    else
                    {
                        await DisplayAlert("Профиль", response.Message, "OK");
                    }
                    activityIndicatorGrid.IsVisible = false;
                }                
            }
            catch(Exception ex)
            {
                await DisplayAlert("Профиль", ex.Message, "OK");
            }
        }

        private bool CanWriteLogin()
        {
            bool canWrite = false;
            string login = entryCellLogin.Text;
            if(login != null && login != _student.Login)
            {
                if (login.Length < 4)
                {
                    throw new Exception("Длина логина не может быть меньше 4 символов!");
                }
                canWrite = true;
            }

            return canWrite;
        }

        private bool CanWriteEmail()
        {
            bool canWrite = false;
            string email = entryCellEmail.Text;
            if (email != null && email != _student.Email)
            {
                if (email.Length < 6)
                {
                    throw new Exception("Длина адреса электронной почты не может быть меньше 6 символов!");
                }
                canWrite = true;
            }

            return canWrite;
        }

        private bool CanWritePass()
        {
            bool canWrite = false;
            string pass = entryCellNewPass.Text;
            if(pass != null && pass != "" && pass != _student.Pass)
            {
                if (entryCellOldPass.Text != _student.Pass)
                {
                    throw new Exception("Не верен старый пароль!");
                }

                if (pass.Length < 4)
                {
                    throw new Exception("Длина пароля не может быть меньше 4 символов!");
                }

                if (pass != entryCellNewPassAgain.Text)
                {
                    throw new Exception("Пароли не совпадают!");
                }

                int strength = PasswordEvaluator.GetStrength(pass);
                if (strength < 2)
                {
                    throw new Exception("Новый пароль слишком слабый!");
                }

                canWrite = true;
            }

            return canWrite;
        }
    }
}