﻿using Mentor.JsonView;
using Mentor.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mentor
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectDiscipPage : ContentPage
	{
        public ObservableCollection<Sem> Sems { get; }
        public ObservableCollection<Year> Years { get; }
        public ObservableCollection<Discipline> Disciplines { get; set; } = new ObservableCollection<Discipline>();
        Student _student;
        RestClient _restClient;

        public SelectDiscipPage (Student stud)
		{
            _restClient = new RestClient();
            _student = stud;

            InitializeComponent ();
            listViewSubj.ItemsSource = Disciplines;
            Years = new ObservableCollection<Year>(Year.GetYears());
            Sems = new ObservableCollection<Sem>(Sem.GetSems());
            pickerYear.ItemsSource = Years;
            pickerSem.ItemsSource = Sems;
            SetCurrentYear();
            SetCurrentSem();

            this.Appearing += ResultsPage_Appearing;
        }

        private void SetCurrentYear()
        {
            pickerYear.SelectedIndex = 0;
        }

        public void SetCurrentSem()
        {
            int curSem = Sem.GetCurrentSemNum();
            pickerSem.SelectedIndex = curSem - 1;
        }

        private bool _isSynchronized = false;
        private async void SyncDiscip()
        {
            activityIndicatorGrid.IsVisible = true;
            _isSynchronized = true;
            Disciplines.Clear();
            List<Discipline> disciplineList = await _restClient.GetDiscipline(_student);

            if ((disciplineList != null) && (disciplineList.Count != 0))
            {
                for (int i = 0; i < disciplineList.Count; i++)
                    Disciplines.Add(disciplineList[i]);
            }
            else
                Disciplines.Add(new Discipline() { Id = -1, Name = "Записи не найдены" });
            activityIndicatorGrid.IsVisible = false;
        }

        private void ResultsPage_Appearing(object sender, EventArgs e)
        {
            if(_isSynchronized == false)
                SyncDiscip();
        }

        private ResultRequest CreateResultRequest(int disciplineId)
        {
            ResultRequest request = new ResultRequest();
            request.DisciplineId = disciplineId;
            request.StudentId = _student.Id;
            request.AccessToken = _student.AccessToken;
            request.Year = Years[pickerYear.SelectedIndex].YearNum;
            request.Sem = Sems[pickerSem.SelectedIndex].SemNum;
            return request;
        }

        private async void buttonResults_Clicked(object sender, EventArgs e)
        {
            if(listViewSubj.SelectedItem != null)
            {
                if(listViewSubj.SelectedItem is Discipline discipline)
                {
                    if (discipline.Id > 0)
                    {
                        activityIndicatorGrid.IsVisible = true;
                        ResultRequest request = CreateResultRequest(discipline.Id);
                        TestResultResponce resultResp = await _restClient.GetTestResults(request);
                        if (resultResp != null &&
                            resultResp.DiaryRecords != null &&
                            resultResp.DiaryRecords.Count > 0
                            )
                        {
                            string bars = "БАРС: " + resultResp.BARS.ToString();
                            string visited = "Посещено: " + resultResp.Visited.ToString() + "%";
                            string val = "Сдано: " + resultResp.Value.ToString() + "%";

                            var resultPage = new NavigationPage(new ResultsPage(resultResp, discipline.Name));
                            await Navigation.PushAsync(resultPage);
                        }
                        else
                        {
                            await DisplayAlert("Результаты", "Записи отсутствуют. Внимание! Данные дневника обновляются на сервере каждые 10 секунд. Не видите своих результатов - подождите 10 секунд.", "OK");
                        }
                        activityIndicatorGrid.IsVisible = false;
                    }
                }
            }
        }

        private void buttonSync_Clicked(object sender, EventArgs e)
        {
            SyncDiscip();
        }
    }
}