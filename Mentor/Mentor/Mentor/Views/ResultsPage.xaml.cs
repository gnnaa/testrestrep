﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Mentor.JsonView;
using Mentor.Models;
using System.Collections.ObjectModel;

namespace Mentor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultsPage : ContentPage
    {
        ObservableCollection<DiaryRecord> _resultsList = new ObservableCollection<DiaryRecord>();
        TestResultResponce _result;
        string _discipName;

        public ResultsPage(TestResultResponce result, string discipName)
        {
            _result = result;
            _discipName = discipName;
            InitializeComponent();

            listView.ItemsSource = _resultsList;

            this.Appearing += ResultsPage_Appearing;
        }

        private void ResultsPage_Appearing(object sender, EventArgs e)
        {
            if(_result != null)
            {
                this.Title = _discipName;
                labelBARS.Text = "БАРС: " + _result.BARS.ToString();
                labelVisited.Text = "Посещено: " + _result.Visited.ToString() + "%";
                labelValue.Text = "Сдано: " + _result.Value.ToString() + "%";

                foreach (var item in _result.DiaryRecords)
                {
                    _resultsList.Add(item);
                }
            }
        }
    }
}