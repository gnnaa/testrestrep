﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.Interfaces;
using Mentor.JsonView;
using Xamarin.Forms;
using System.IO;

namespace Mentor.Models
{
    public static class Synchronizer
    {
        public static string FileName { get; set; }
        public static string SharedSec { get; set; }

        public static void UpdateAccessToken(Student student, List<AnswerData> answers)
        {
            foreach (var item in answers)
            {
                item.AccessToken = student.AccessToken;
            }
        }

        public static async Task<Response> Synchronize(Student student)
        {
            Response response = new Response();
            response.Id = -1; // если метод возвратит  response.Id = 0, то синхронизировать нечего; -1 - ошибка; 1 - данные успешно синхронизированы

            //проверяем наличие файла
            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), FileName);
            bool b = File.Exists(fileName);
            if (b == true) //файл есть
            {
                //берем зиписи из локальной базы
                string sdataAes = File.ReadAllText(fileName);
                try
                {
                    string sdata = CryptoAesStandard.Decrypt(sdataAes, SharedSec);
                    List<AnswerData> localAnswerDataList = JsonConvert.DeserializeObject<List<AnswerData>>(sdata);
                    if (localAnswerDataList.Count > 0) //если есть что записывать в удаленную
                    {
                        UpdateAccessToken(student, localAnswerDataList);
                        RestClient restClient = new RestClient();
                        response = await restClient.WriteAnswerData(localAnswerDataList);
                        if (response.Id > 0) //если записали, то локальную очищаем
                        {
                            List<AnswerData> list = new List<AnswerData>();
                            string str = JsonConvert.SerializeObject(list);
                            string strAes = CryptoAesStandard.Encrypt(str, SharedSec);
                            File.WriteAllText(fileName, strAes);
                        }
                    }
                    else
                    {
                        response.Id = 0;
                    }
                }
                catch(Exception e)
                {
                    List<AnswerData> list = new List<AnswerData>();
                    string str = JsonConvert.SerializeObject(list);
                    string strAes = CryptoAesStandard.Encrypt(str, SharedSec);
                    File.WriteAllText(fileName, strAes);
                    response.Id = 0;
                }
            }
            else
            {
                response.Id = 0;
            }
            return response;
        }

        public static void WriteAnswerDataToLocal(AnswerData answerData)
        {
            //проверяем наличие файла
            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), FileName);
            bool b = File.Exists(fileName);
            if (b == true) //файл есть
            {
                //берем зиписи из локальной базы
                try
                {
                    string sdataAes = File.ReadAllText(fileName);
                    string sdata = CryptoAesStandard.Decrypt(sdataAes, SharedSec);
                    List<AnswerData> localRecords = JsonConvert.DeserializeObject<List<AnswerData>>(sdata);
                    localRecords.Add(answerData);
                    string str = JsonConvert.SerializeObject(localRecords);
                    string strAes = CryptoAesStandard.Encrypt(str, SharedSec);
                    File.WriteAllText(fileName, strAes);
                }
                catch(Exception e)
                {
                    List<AnswerData> localRec = new List<AnswerData>();
                    localRec.Add(answerData);
                    string str = JsonConvert.SerializeObject(localRec);
                    string strAes = CryptoAesStandard.Encrypt(str, SharedSec);
                    File.WriteAllText(fileName, strAes);
                }                
            }
            else //файла нет
            {
                List<AnswerData> localRec = new List<AnswerData>();
                localRec.Add(answerData);
                string str = JsonConvert.SerializeObject(localRec);
                string strAes = CryptoAesStandard.Encrypt(str, SharedSec);
                File.WriteAllText(fileName, strAes);
            }
        }
    }
}
