﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Mentor.Models
{
    public class ImageButton
    {
        private bool _isOn = false;
        private Image _image;
        private ImageSource _onSource;
        private ImageSource _offSource;

        public ImageButton(Image im, ImageSource on, ImageSource off)
        {
            _image = im;
            _onSource = on;
            _offSource = off;
            _image.Source = _offSource;
        }

        public bool IsOn
        {
            get
            {
                return _isOn;
            }
            set
            {
                if(value == true)
                {
                    if(_isOn == false)
                    {
                        _isOn = true;
                        _image.Source = _onSource;
                    }
                }
                else
                {
                    if (_isOn == true)
                    {
                        _isOn = false;
                        _image.Source = _offSource;
                    }
                }
            }
        }
    }
}
