﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Mentor.Models
{
    public class Sem
    {
        public Sem(int semNum)
        {
            SemNum = semNum;
        }
        public int SemNum { get; }
        public override string ToString()
        {
            if(SemNum == 1)
                return $"Первый семестр";
            else
                return $"Второй семестр";
        }
        public static int GetCurrentSemNum()
        {
            int month = DateTime.Now.Month;
            int sem = 1;
            if (month > 1 && month < 9)
                sem = 2;
            return sem;
        }
        public static IEnumerable<Sem> GetSems()
        {
            List<Sem> sems = new List<Sem>();
            sems.Add(new Sem(1));
            sems.Add(new Sem(2));
            return sems;
        }
    }

    public class Year
    {
        public Year(int year)
        {
            YearNum = year;
        }
        public int YearNum { get; }
        public static int GetCurrentYearNum()
        {
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            if(month < 9)
                year--;
            return year;
        }
        public static IEnumerable<Year> GetYears()
        {
            int curYear = GetCurrentYearNum();
            List<Year> years = new List<Year>();
            for (int i = curYear; i > curYear - 5; i--)
                years.Add(new Year(i));
            return years;
        }
        public override string ToString()
        {
            return $"{YearNum}/{YearNum + 1}";
        }
    }
}