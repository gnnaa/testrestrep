﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.Interfaces;
using Mentor.JsonView;
using Xamarin.Forms;
using System.IO;

namespace Mentor.Models
{
    public static class AutorizationSubstitute
    {
        const string FileName = "AData";

        public static AuthorizationRequest GetAutorizationData()
        {
            AuthorizationRequest request = null;
            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), FileName);
            bool b = File.Exists(fileName);
            if (b == true) //файл есть
            {
                //берем зиписи из локальной базы
                string sdata = File.ReadAllText(fileName);
                request = JsonConvert.DeserializeObject<AuthorizationRequest>(sdata);
            }
            return request;
        }

        public static void WriteAutorizationData(AuthorizationRequest request)
        {
            string str = JsonConvert.SerializeObject(request);
            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), FileName);
            File.WriteAllText(fileName, str);
        }
    }
}
