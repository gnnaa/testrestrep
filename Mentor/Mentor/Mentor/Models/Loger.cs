﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.Interfaces;
using Xamarin.Forms;
using System.IO;

namespace Mentor.Models
{
    public static class Loger
    {
        const string FileName = "LData";

        private static void WriteToLocal(LogRecord record)
        {
            //проверяем наличие файла
            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), FileName);
            bool b = File.Exists(fileName);
            if (b == true) //файл есть
            {
                //берем зиписи из локальной базы
                string sdata = File.ReadAllText(fileName);
                List<LogRecord> localRecords = JsonConvert.DeserializeObject<List<LogRecord>>(sdata);
                localRecords.Add(record);
                string str = JsonConvert.SerializeObject(localRecords);
                File.WriteAllText(fileName, str);
            }
            else //файла нет
            {
                List<LogRecord> localRec = new List<LogRecord>();
                localRec.Add(record);
                string str = JsonConvert.SerializeObject(localRec);
                File.WriteAllText(fileName, str);
            }
        }
        
        public static void Write(string mess)
        {
            LogRecord record = new LogRecord(mess);
            WriteToLocal(record);
        }

        public static void Write(Exception e)
        {
            LogRecord record = new LogRecord(e);
            WriteToLocal(record);
        }

        private static void WriteToRemote()
        {
            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), FileName);
            bool b = File.Exists(fileName);
            if (b == true) //файл есть
            {
                //берем зиписи из локальной базы
                string sdata = File.ReadAllText(fileName);
                List<LogRecord> localRecords = JsonConvert.DeserializeObject<List<LogRecord>>(sdata);
                string str = JsonConvert.SerializeObject(localRecords);
            }
        }
    }
}
