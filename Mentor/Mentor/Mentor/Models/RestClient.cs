﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json.Linq;
using System.IO;
using Mentor.Models;
using Mentor.JsonView;
using Xamarin.Forms;

namespace Mentor
{
    public class RestClient
    {
        const string UrlAutorisation = "https://testservermentor3.h1n.ru/app_autorization.php";
        const string UrlGetTestBlank = "https://testservermentor3.h1n.ru/app_get_test.php";
        const string UrlWriteAnswerData = "https://testservermentor3.h1n.ru/app_write_test_result.php";
        const string UrlGetDiscipline = "https://testservermentor3.h1n.ru/app_get_discipline.php";
        const string UrlGetTestResults = "https://testservermentor3.h1n.ru/app_get_test_result.php";
        const string UrlSetNewProfile = "https://testservermentor3.h1n.ru/app_set_new_profile.php";

        //const string UrlAutorisation = "https://mentor.h1n.ru/app_autorization.php";
        //const string UrlGetTestBlank = "https://mentor.h1n.ru/app_get_test.php";
        //const string UrlWriteAnswerData = "https://mentor.h1n.ru/app_write_test_result.php";
        //const string UrlGetDiscipline = "https://mentor.h1n.ru/app_get_discipline.php";
        //const string UrlGetTestResults = "https://mentor.h1n.ru/app_get_test_result.php";
        //const string UrlSetNewProfile = "https://mentor.h1n.ru/app_set_new_profile.php";

        // настройка клиента
        private static HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            return client;
        }

        public static async Task<Response> SetNewProfile(Student stud)
        {
            HttpClient client = GetClient();
            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(UrlSetNewProfile, new StringContent(JsonConvert.SerializeObject(stud), Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {
                //Loger.Write(e);
            }

            if (response != null)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Response resp = new Response();
                    resp.Id = -1;
                    resp.Message = "Ошибка соединения! Проверьте подключение к интернету!";
                    //Loger.Write(response.ReasonPhrase);
                    return resp;
                }
                else
                {
                    string res = await response.Content.ReadAsStringAsync();
                    Response resp = JsonConvert.DeserializeObject<Response>(res);
                    return resp;
                }
            }
            else
            {
                Response resp = new Response();
                resp.Id = -1;
                resp.Message = "Ошибка соединения! Проверьте подключение к интернету!";
                return resp;
            }
        }


        public async Task<Response> GetTestBlank(Student stud)
        {
            Response st = stud;
            HttpClient client = GetClient();
            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(UrlGetTestBlank, new StringContent(JsonConvert.SerializeObject(st), Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {
                //Loger.Write(e);
            }

            if (response != null)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Response resp = new Response();
                    resp.Id = -1;
                    resp.Message = "Ошибка соединения! Проверьте подключение к интернету!";
                    //Loger.Write(response.ReasonPhrase);
                    return resp;
                }
                else
                {
                    string res = await response.Content.ReadAsStringAsync();
                    Response resp = JsonConvert.DeserializeObject<TestBlank>(res);
                    return resp;
                }
            }
            else
            {
                Response resp = new Response();
                resp.Id = -1;
                resp.Message = "Ошибка соединения! Проверьте подключение к интернету!";
                return resp;
            }
        }

        private int _tryCount = 0;
        private async Task<HttpResponseMessage> RecursivePostAsync(string url, string data)
        {
            HttpClient client = GetClient();
            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(url, new StringContent(data, Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {
                if(_tryCount < 3)
                {
                    _tryCount++;
                    response = await RecursivePostAsync(url, data);
                }
                else
                {
                    //Loger.Write(e);
                }
            }
            if (response != null)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    if (_tryCount < 3)
                    {
                        _tryCount++;
                        response = await RecursivePostAsync(url, data);
                    }
                }
            }
            return response;
        }
        
        public async Task<Response> GetStudent(AuthorizationRequest request)
        {
            string data = JsonConvert.SerializeObject(request);
            HttpResponseMessage response = null;
            _tryCount = 0;
            response = await RecursivePostAsync(UrlAutorisation, data);

            if(response != null)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Response resp = new Response();
                    resp.Id = -1;
                    resp.Message = "Ошибка соединения! Проверьте подключение к интернету!";
                    //Loger.Write(response.ReasonPhrase);
                    return resp;
                }
                else
                {
                    string res = await response.Content.ReadAsStringAsync();
                    Response resp = JsonConvert.DeserializeObject<Response>(res);
                    if (resp.Id > -1)
                    {
                        return JsonConvert.DeserializeObject<Student>(res);
                    }
                    else
                    {
                        return resp;
                    }
                }
            }
            else
            {
                Response resp = new Response();
                resp.Id = -1;
                resp.Message = "Ошибка соединения! Проверьте подключение к интернету!";
                return resp;
            }
        }

        public async Task<Response> WriteAnswerData(List<AnswerData> answerData)
        {
            HttpClient client = GetClient();
            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(UrlWriteAnswerData, new StringContent(JsonConvert.SerializeObject(answerData), Encoding.UTF8, "application/json"));
            }
            catch(Exception e)
            {
                //Loger.Write(e);
            }

            if (response != null)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Response resp = new Response();
                    resp.Id = -1;
                    resp.Message = "Ошибка соединения! Проверьте подключение к интернету!";
                    //Loger.Write(response.ReasonPhrase);
                    return resp;
                }
                else
                {
                    string res = await response.Content.ReadAsStringAsync();
                    Response resp = JsonConvert.DeserializeObject<Response>(res);
                    return resp;
                }
            }
            else
            {
                Response resp = new Response();
                resp.Id = -1;
                resp.Message = "Ошибка соединения! Проверьте подключение к интернету!";
                return resp;
            }
        }

        public async Task<List<Discipline>> GetDiscipline(Student student)
        {
            HttpClient client = GetClient();
            List<Discipline> list = null;
            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(UrlGetDiscipline, new StringContent(JsonConvert.SerializeObject(student), Encoding.UTF8, "application/json"));
            }
            catch(Exception e)
            {

            }

            if (response != null)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string res = await response.Content.ReadAsStringAsync();
                    try
                    {
                        list = JsonConvert.DeserializeObject<List<Discipline>>(res);
                    }
                    catch(Exception e)
                    {
                        list = null;
                    }
                }
            }
            
            return list;
        }

        public async Task<TestResultResponce> GetTestResults(ResultRequest request)
        {
            HttpClient client = GetClient();

            TestResultResponce resultResp = null;
            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(UrlGetTestResults, new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {

            }

            if (response != null)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string res = await response.Content.ReadAsStringAsync();
                    try
                    {
                        resultResp = JsonConvert.DeserializeObject<TestResultResponce>(res);
                    }
                    catch (Exception e)
                    {

                    }
                }
            }

            return resultResp;
        }
    }
}
